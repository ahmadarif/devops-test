## Description

Repository for DevOps Testing

## Prerequisite
- NodeJS
- Database Engine (MySQL, MariaDB, PostgreSQL, etc)
- Redis
- Logstash (ELK Stack)

## Configurations
- Copy file configuration from template
  ```bash
  $ cp .env.example .env
  ```
- Edit your own config variables

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run serve

# build
$ npm run build

# production mode
$ npm run start
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
