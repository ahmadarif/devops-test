import { Injectable, Logger } from '@nestjs/common';
import * as Logstash from 'logstash-client';
import { Config } from '../helpers/config.helper';

@Injectable()
export class LogstashService {
  logstash: Logstash;

  constructor() {
    this.logstash = new Logstash({
      type: Config.get('LOGSTASH_TYPE'),
      host: Config.get('LOGSTASH_HOST'),
      port: Config.getNumber('LOGSTASH_PORT'),
    });
  }

  log(message: string) {
    const payload = { level: 'info', message: message };
    this.logstash.send(payload);
    Logger.log(JSON.stringify(payload));
  }

  warn(message: string) {
    const payload = { level: 'warn', message: message };
    this.logstash.send(payload);
    Logger.warn(JSON.stringify(payload));
  }

  error(message: string, stacktrace?: any) {
    const payload = { level: 'error', message: message, stackTrace: stacktrace };
    this.logstash.send(payload);
    Logger.error(JSON.stringify(payload));
  }
}
