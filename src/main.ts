import { ApplicationContext } from './app.context';
import { ConfigService } from './modules/config/config.service';

async function bootstrap() {
  const app = await ApplicationContext();

  // convert undefined response field to null
  app.set('json replacer', (_, value) => {
    return typeof value === 'undefined' ? null : value;
  });

  await app.listen(app.get(ConfigService).getInt('APP_PORT'));
}

bootstrap();
