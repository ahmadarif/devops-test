import { Body, Controller, Get, Post, Query, Res } from '@nestjs/common';
import * as Redis from 'ioredis';
import { ConfigService } from '../modules/config/config.service';

@Controller('redis')
export class RedisController {
  redis: Redis.Redis;

  constructor(private config: ConfigService) {
    if (this.config.get('REDIS_PASSWORD')) {
      this.redis = new Redis({
        host: this.config.get('REDIS_HOST'),
        port: this.config.getInt('REDIS_PORT'),
        password: this.config.get('REDIS_PASSWORD'),
      });
    } else {
      this.redis = new Redis({
        host: this.config.get('REDIS_HOST'),
        port: this.config.getInt('REDIS_PORT'),
      });
    }
  }

  @Get()
  async get(@Res() res, @Query() query) {
    const key: string = query.key;
    const value = await this.redis.get(key);

    const data = {};
    data[key] = value;

    return res.json(data);
  }

  @Post()
  set(@Res() res, @Body() body) {
    const key: string = body.key;
    const value: string = body.value;
    this.redis.set(key, value);
    return res.json({ message: 'Data has been set' });
  }
}
