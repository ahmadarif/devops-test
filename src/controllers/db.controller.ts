import { Body, Controller, Delete, Get, Param, Post, Put, Req } from '@nestjs/common';
import { User } from '../models/user';

@Controller('dbs')
export class DbController {
  @Get()
  async getAll() {
    return await User.find();
  }

  @Get(':id')
  async getById(@Req() req, @Param('id') id) {
    const user = await User.findOne(id);
    if (!user) return req.res.status(404).json({ message: 'Data not found' });
    return user;
  }

  @Post()
  async insert(@Body() body) {
    const name: string = body.name;
    const age: number = body.age;

    const user = new User();
    user.name = name;
    user.age = age;
    await user.save();

    return user;
  }

  @Put(':id')
  async update(@Req() req, @Param('id') id, @Body() body) {
    const name: string = body.name;
    const age: number = body.age;

    const user = await User.findOne(id);
    if (!user) return req.res.status(404).json({ message: 'Data not found' });

    if (name) user.name = name;
    if (age) user.age = age;
    await user.save();

    return user;
  }

  @Delete(':id')
  async delete(@Req() req, @Param('id') id) {
    const user = await User.findOne(id);
    if (!user) return req.res.status(404).json({ message: 'Data not found' });

    await user.remove();
    return { message: 'user deleted' };
  }
}
