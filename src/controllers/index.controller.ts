import { Controller, Get } from '@nestjs/common';
import { AppService } from '../services/app.service';
import { LogstashService } from '../services/logstash.service';

@Controller()
export class IndexController {
  constructor(
    private appService: AppService,
    private logger: LogstashService,
  ) {}

  @Get()
  getHello(): string {
    this.logger.log('sample-' + Math.round(Math.random() * 1000));
    return this.appService.getHello();
  }
}
