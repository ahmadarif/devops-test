import { Module } from '@nestjs/common';
import { DbController } from './controllers/db.controller';
import { IndexController } from './controllers/index.controller';
import { RedisController } from './controllers/redis.controller';
import { ConfigModule } from './modules/config/config.module';
import { DatabaseModule } from './modules/database/database.module';
import { AppService } from './services/app.service';
import { LogstashService } from './services/logstash.service';

@Module({
  imports: [
    ConfigModule,
    DatabaseModule,
  ],
  controllers: [
    IndexController,
    DbController,
    RedisController,
  ],
  providers: [
    AppService,
    LogstashService,
  ],
})
export class AppModule {}
