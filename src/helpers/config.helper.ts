import * as dotenv from 'dotenv';
dotenv.config();

export class Config {
  static get(key: string): string {
    return process.env[key];
  }

  static getNumber(key: string): number {
    return parseInt(process.env[key], 10);
  }

  static getBoolean(key: string): boolean {
    return process.env[key] === 'true';
  }

  static getObject(key: string): any {
    if (process.env[key]) {
      return JSON.parse(process.env[key]);
    } else {
      return null;
    }
  }
}
